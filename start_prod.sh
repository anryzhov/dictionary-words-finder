#!/bin/bash

. .env
. .funcs

APP_ENV=prod
LOCAL_PORT=127.0.0.1:8080
CONTAINER_NAME=$(container_name)
PRODUCTION_IMAGE=$(production_image ${SHORT_NAME})
USER=${APP_USER:-root}
PORT=${APP_PORT:-80}

# -- check for already started container for this application --
CONTAINER_ID=`docker ps -q --filter="name=${CONTAINER_NAME}"`
if [ -n "${CONTAINER_ID}" ]; then
	echo "stop container => `docker container stop ${CONTAINER_ID}`"
fi

echo "run production image => \"${PRODUCTION_IMAGE}\" in container => \"${CONTAINER_NAME}\" under user => \"${USER}\""
docker run --rm -p ${LOCAL_PORT}:${PORT}/tcp \
	-v ${PWD}/include:${DICTIONARY_PATH}:ro \
	--name ${CONTAINER_NAME} \
	--user ${USER} ${PRODUCTION_IMAGE}
