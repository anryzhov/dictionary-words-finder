Scrabble Solver build docker image and kubernetes deployment script.

Use these script:

	build_prod.sh - for build docker image
	deploy_prod.sh - for deploy on k8s cluster
	start_prod.sh - for run production image on  localhost


---- DEPRICATED BUT ACTUAL NOTE ---------------------------------------------
make link /home/webs/domains/wfs/app/include to ./include in this folder,
because solver search dictionares here -> /home/webs/domains/wfs/app/include
-----------------------------------------------------------------------------
