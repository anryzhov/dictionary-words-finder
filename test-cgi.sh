#!/bin/bash

rm -rf output/*

START_TIME=$(date  +%s)
for i in {0..4999}
do
   #echo "#$i request =>"
   curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -d @test_data  \
       https://www.wordsforscrabble.com/cgi-bin/scrabble-solver?i=${i} > output/response-${i}.log 2> output/err-${i}.log &
   #sleep 1
done
END_TIME=$(date  +%s)

wait

echo "Total test time:  $(( END_TIME-START_TIME ))"
