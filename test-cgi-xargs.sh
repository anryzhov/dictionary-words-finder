#!/bin/bash

rm -rf output/*

START_TIME=$(date  +%s)


seq 0 4999 | xargs -I {} -n1 -P2000  \
    sh -c 'curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -d @test_data https://www.wordsforscrabble.com/cgi-bin/scrabble-solver?i=$1   > output/response-$1.log 2> output/err-$1.log' -- {}


END_TIME=$(date  +%s)


echo "Total test time:  $(( END_TIME-START_TIME ))"
